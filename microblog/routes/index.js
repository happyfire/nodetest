module.exports = function(app){
	app.get('/', function(req,res){
		res.render('index',{
			title:'Home'
		});
	});
	
	app.get('/reg', function(req,res){
		res.render('reg',{
			title:'用户注册'
		});
	});
	
	app.post('/reg',function(req,res){
		if(req.body['password-repeat']!=req.body['password']){
			req.flash('error','两次输入的口令不一致');
			return res.redirect('/req');
		}
		
		var md5 = crypto.createHash('md5');
		var password = md5.update(req.body.password).digest('base64');
		var newUser = new User({
			name:req.body.username,
			password:password,
		});
		
		User.get(newUser.name, function(err,user){
			if(user)
				err='Username already exists.';
			if(err){
				req.flash('error',err);
				retrun res.redirect('/req');
			}
			
			newUser.save(function(err){
				if(err){
					req.flash('error',err);
					return res.redirect('/req');
				}
				req.session.user = newUser;
				req.flash('success', '注册成功');
				res.redirect('/');
			});
		});
	});
	
	return app.router;
};

